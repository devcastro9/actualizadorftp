USE [ADMIN_EMPRESA]
GO

DECLARE @cuenta_anterior VARCHAR(40) = '2015046557-03-054'
DECLARE @cuenta_nueva VARCHAR(40) = '201-5046557-3-54'
-- Llave primaria
UPDATE dbo.fc_cuenta_bancaria SET cta_codigo = @cuenta_nueva WHERE cta_codigo = @cuenta_anterior
-- Extractos
UPDATE dbo.fo_extracto_BCP SET cuenta = @cuenta_nueva WHERE cuenta = @cuenta_anterior
UPDATE dbo.fo_extracto_ingreso_GRAL SET cuenta = @cuenta_nueva WHERE cuenta = @cuenta_anterior
UPDATE dbo.fo_extracto_egreso_GRAL SET cuenta = @cuenta_nueva WHERE cuenta = @cuenta_anterior
-- Otras tablas
UPDATE dbo.ao_ventas_cobranza_det SET cta_codigo = @cuenta_nueva WHERE cta_codigo = @cuenta_anterior
UPDATE dbo.fo_traspaso_bancos SET cta_codigo = @cuenta_nueva WHERE cta_codigo = @cuenta_anterior
UPDATE dbo.fo_traspaso_bancos SET cta_codigo_destino = @cuenta_nueva WHERE cta_codigo_destino = @cuenta_anterior
UPDATE dbo.fo_traspaso_bancos_Egresos SET cta_codigo = @cuenta_nueva WHERE cta_codigo = @cuenta_anterior
UPDATE dbo.fo_traspaso_bancos_Egresos SET cta_codigo_destino = @cuenta_nueva WHERE cta_codigo_destino = @cuenta_anterior
UPDATE dbo.fo_recibos_detalle SET cta_codigo = @cuenta_nueva WHERE cta_codigo = @cuenta_anterior
UPDATE dbo.fo_recibos_detalle SET cta_codigo_origen = @cuenta_nueva WHERE cta_codigo_origen = @cuenta_anterior
UPDATE dbo.fo_recibos_detalle SET cta_codigo_destino = @cuenta_nueva WHERE cta_codigo_destino = @cuenta_anterior
UPDATE dbo.fo_recibos_detalle_egresos SET cta_codigo = @cuenta_nueva WHERE cta_codigo = @cuenta_anterior
UPDATE dbo.fo_recibos_detalle_egresos SET cta_codigo_origen = @cuenta_nueva WHERE cta_codigo_origen = @cuenta_anterior
UPDATE dbo.fo_recibos_detalle_egresos SET cta_codigo_destino = @cuenta_nueva WHERE cta_codigo_destino = @cuenta_anterior