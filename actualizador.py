from ftplib import FTP_TLS
from os import path, mkdir, chdir
from subprocess import run, Popen
from shutil import rmtree
from py7zr import SevenZipFile
from hurry.filesize import size as sz
# Variables
RUTA = 'D:\\SIGCGI'
DESCARGA = 'D:\\SIGCGI_TEMP'
HOST_IP = 'ftp.cgebolivia.com'
USER = 'sofiauser@cgebolivia.com'
PWD = 'M&w#YE!+9Er1'
ARCHIVO_FTP = 'SIGCGI.7z'

def Actualizar() -> None:
    """
        Programa en python para actualizar el sistema SOFIA
    """
    # Variables de rutas
    path_descarga_ftp = DESCARGA + '\\' + ARCHIVO_FTP
    path_actualizacion = DESCARGA + '\\' + 'SIGCGI'
    # Variables de ordenes cmd
    cerrar_proceso = ['taskkill', '/im', 'SOFIA.exe', '/f', '/t']
    actualizar = ['robocopy', path_actualizacion, RUTA, '*.*', '/s', '/v']
    iniciar_sofia = ['SOFIA.exe']
    # Borrar y crear la carpeta de descarga
    print('Iniciando actualizacion...')
    if path.exists(DESCARGA):
        rmtree(DESCARGA)
    mkdir(DESCARGA)
    # Crear carpeta 
    if not path.exists(RUTA):
        mkdir(RUTA)
    chdir(RUTA)
    # Conectarse y descargar
    print('Iniciando conexion FTP')
    with FTP_TLS(host=HOST_IP, user=USER, passwd=PWD) as ftp:
        try:
            print("Descargando... " + ARCHIVO_FTP + ' Tamaño: ' + sz(ftp.size(ARCHIVO_FTP)))
            ftp.retrbinary("RETR " + ARCHIVO_FTP, open(path_descarga_ftp, 'wb').write)
        except Exception as ex:
            print(ex)
            return
    print('Conexion FTP cerrada')
    # Descomprimir
    print('Extrayendo...' + ARCHIVO_FTP)
    with SevenZipFile(path_descarga_ftp, 'r') as compress:
        compress.extractall(path_actualizacion)
    print('Extraccion completa')
    # Ejecutar copiado y actualizacion
    print('Etapa final de actualizacion')
    try:
        run(cerrar_proceso)
        run(actualizar)
        rmtree(DESCARGA)
        print('Actualizacion finalizada')
        Popen(iniciar_sofia)
    except Exception as ex:
        print(ex)
        return

if __name__ == '__main__':
    Actualizar()